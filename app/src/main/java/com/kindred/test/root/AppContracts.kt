package com.kindred.test.root

import io.reactivex.disposables.Disposable

interface BaseView<T : BaseViewModel> {
    fun getAssociatedViewModel(): T
}

interface BaseViewModel {
    fun manageActionDisposables(vararg disposable: Disposable)
}
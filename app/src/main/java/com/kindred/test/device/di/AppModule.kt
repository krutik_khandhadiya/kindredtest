package com.kindred.test.device.di

import android.app.Application
import android.content.Context
import dagger.Binds

import javax.inject.Singleton

import dagger.Module

@Module
abstract class AppModule {
    @Binds
    @Singleton
    internal abstract fun provideContext(application: Application): Context
}

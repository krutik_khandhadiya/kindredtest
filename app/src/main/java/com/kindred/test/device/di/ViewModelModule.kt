package com.kindred.test.device.di

import androidx.lifecycle.ViewModelProvider
import com.kindred.test.presentation.games.GamesViewModelImpl
import com.kindred.test.root.BaseViewModel
import com.kindred.test.root.ViewModelFactory
import com.kindred.test.root.ViewModelKey

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(GamesViewModelImpl::class)
    protected abstract fun bindGamesViewModelImpl(gamesViewModelImpl: GamesViewModelImpl): BaseViewModel
}
package com.kindred.test.device.di


import com.kindred.test.presentation.games.GameActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindGameActivity(): GameActivity
}
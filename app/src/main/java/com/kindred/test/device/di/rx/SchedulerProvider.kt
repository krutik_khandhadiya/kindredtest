package com.kindred.test.device.di.rx

import io.reactivex.Scheduler

interface SchedulerProvider {
    fun provideSchedulerProvider(): Scheduler
}
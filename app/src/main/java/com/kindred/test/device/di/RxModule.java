package com.kindred.test.device.di;

import com.kindred.test.device.di.rx.*;
import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

import javax.inject.Singleton;

@Module
public final class RxModule {

    private RxModule() {

    }
    @Provides
    static CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    @MainThreadScheduler
    static SchedulerProvider provideMainThreadSchedulerProvider() {
        return new MainThreadSchedulerProvider();
    }

    @Provides
    @Singleton
    @IoThreadScheduler
    static SchedulerProvider provideIoSchedulerProvider() {
        return new IoThreadSchedulerProvider();
    }
}
package com.kindred.test.service

import com.kindred.test.domain.games.model.GameResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiService {

    @GET("getGamesByMarketAndDevice.json")
    fun getGameInfo(@QueryMap options: Map<String, String>): Flowable<GameResponse>
}
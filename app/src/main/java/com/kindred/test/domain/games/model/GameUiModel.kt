package com.kindred.test.domain.games.model

data class GameUiModel(val name: String?, val imageUrl: String?)
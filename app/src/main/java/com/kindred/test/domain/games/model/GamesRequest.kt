package com.kindred.test.domain.games.model

data class GamesRequest(
    val jurisdiction: String = "UK",
    val brand: String = "unibet",
    val deviceGroup: String = "mobilephone",
    val locale: String = "en_GB",
    val currency: String = "GBP",
    val categories: String = "casino,softgames",
    val clientId: String = "casinoapp"
) {
    fun getQueryMap(): Map<String, String> {
        val map = HashMap<String, String>()
        map.put("jurisdiction", jurisdiction)
        map.put("deviceGroup", deviceGroup)
        map.put("brand", brand)
        map.put("locale", locale)
        map.put("currency", currency)
        map.put("categories", categories)
        map.put("clientId", clientId)
        return map
    }
}
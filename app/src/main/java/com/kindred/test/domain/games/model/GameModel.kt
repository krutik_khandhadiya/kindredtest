package com.kindred.test.domain.games.model

import com.google.gson.annotations.SerializedName

data class GameModel(@SerializedName("gameId")
                     val gameId: String?,
                     @SerializedName("launchLocale")
                     val launchLocale: String?,
                     @SerializedName("subVendor")
                     val subVendor: SubVendor,
                     @SerializedName("typeSettings")
                     val typeSettings: TypeSettings?,
                     @SerializedName("vendorId")
                     val vendorId: String?,
                     @SerializedName("playUrl")
                     val playUrl: String?,
                     @SerializedName("tags")
                     val tags: List<String>?,
                     @SerializedName("licenses")
                     val licenses: List<String>?,
                     @SerializedName("gameName")
                     val gameName: String?,
                     @SerializedName("vendor")
                     val vendor: Vendor?,
                     @SerializedName("imageUrl")
                     val imageUrl: String?,
                     @SerializedName("displayVendorName")
                     val displayVendorName: String?,
                     @SerializedName("backgroundImageUrl")
                     val backgroundImageUrl: String?)
package com.kindred.test.domain.games.model

import com.google.gson.annotations.SerializedName

data class GameResponse(@SerializedName("games") val games: Map<String, GameModel>)
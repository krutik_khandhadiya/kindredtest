package com.kindred.test.domain.games.usecase


import com.kindred.test.data.source.games.GamesDataRepository
import com.kindred.test.device.di.rx.IoThreadScheduler
import com.kindred.test.device.di.rx.MainThreadScheduler
import com.kindred.test.device.di.rx.SchedulerProvider
import com.kindred.test.domain.games.model.GameResponse
import com.kindred.test.domain.games.model.GameUiModel
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.root.UseCase
import io.reactivex.Flowable
import javax.inject.Inject

class GetGameDataUseCase @Inject constructor(private val gamesDataRepository: GamesDataRepository, @IoThreadScheduler subscribeOnScheduler: SchedulerProvider, @MainThreadScheduler observeOnScheduler: SchedulerProvider) :
    UseCase<GamesRequest, List<GameUiModel>>() {
    override fun createObservable(request: GamesRequest): Flowable<List<GameUiModel>> {
        return gamesDataRepository.getGameData(request).map { game: GameResponse -> game.games }
            .map { games ->
                games.values.map { gameModel ->
                    GameUiModel(gameModel.gameName, gameModel.imageUrl)
                }
            }
    }

}
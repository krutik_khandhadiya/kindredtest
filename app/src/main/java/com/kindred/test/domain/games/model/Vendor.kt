package com.kindred.test.domain.games.model

import com.google.gson.annotations.SerializedName

data class Vendor(@SerializedName("name")
                  val name: String?,
                  @SerializedName("vendorGroupId")
                  val vendorGroupId: String?)
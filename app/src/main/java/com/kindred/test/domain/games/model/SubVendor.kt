package com.kindred.test.domain.games.model

import com.google.gson.annotations.SerializedName

data class SubVendor(@SerializedName("name")
                     val name: String?,
                     @SerializedName("id")
                     val id: String?)
package com.kindred.test.domain.games.model

import com.google.gson.annotations.SerializedName

data class TypeSettings(@SerializedName("returnToPlayer")
                        val returnToPlayer: Double?,
                        @SerializedName("noOfReels")
                        val noOfReels: Int?,
                        @SerializedName("maxLines")
                        val maxLines: String?,
                        @SerializedName("noOfRows")
                        val noOfRows: Int?,
                        @SerializedName("volatility")
                        val volatility: String?)
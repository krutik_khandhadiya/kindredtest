package com.kindred.test.presentation.games

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.kindred.test.domain.games.model.GameUiModel
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.domain.games.usecase.GetGameDataUseCase
import com.kindred.test.presentation.data.Resource
import com.kindred.test.presentation.util.extensions.emptyData
import com.kindred.test.presentation.util.extensions.error
import com.kindred.test.presentation.util.extensions.loading
import com.kindred.test.presentation.util.extensions.success
import com.kindred.test.root.BaseViewModelImpl
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GamesViewModelImpl @Inject constructor(
    private val getGameDataUseCase: GetGameDataUseCase,
    compositeDisposable: CompositeDisposable
) :
    BaseViewModelImpl(compositeDisposable), GamesViewModel {

    private val gamesLiveData = MutableLiveData<Resource<List<GameUiModel>>>()

    override fun getGamesData(gamesRequest: GamesRequest): LiveData<Resource<List<GameUiModel>>> {
        gamesLiveData.loading()
        manageActionDisposables(getGameDataUseCase.execute(gamesRequest).subscribe({ response: List<GameUiModel>? ->
            response?.let {
                gamesLiveData.success(response)
            }
            if (response == null)
                gamesLiveData.emptyData("")
        }, { t: Throwable? ->
            t?.let {
                gamesLiveData.error(t, "Something went wrong")
            }
        }
        ))
        return gamesLiveData
    }
}
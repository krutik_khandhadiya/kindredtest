package com.kindred.test.presentation.games

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.kindred.test.R
import com.kindred.test.domain.games.model.GameUiModel
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.presentation.data.ResourceState
import com.kindred.test.presentation.games.adapter.GameListAdapter
import com.kindred.test.root.BaseActivity
import kotlinx.android.synthetic.main.activity_game.*
import javax.inject.Inject

class GameActivity : BaseActivity<GamesViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    var gamesList = mutableListOf<GameUiModel>()

    override fun getAssociatedViewModel(): GamesViewModel {
        return ViewModelProviders.of(this, viewModelFactory).get(GamesViewModelImpl::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
        txtEmpty.visibility = GONE
        progressLoad.visibility = VISIBLE
        rvGames.layoutManager = GridLayoutManager(this, 2)

        rvGames.adapter = GameListAdapter(gamesList)



        viewModel.getGamesData(GamesRequest()).observe(this, Observer { data ->
            data?.let { handleDataState(it.status, it.data, it.message) }
        })


    }

    private fun handleDataState(resourceState: ResourceState, data: List<GameUiModel>?, message: String?) {
        when (resourceState) {
            ResourceState.LOADING -> setupScreenForLoadingState(true)
            ResourceState.SUCCESS -> setupSuccess(data)

            ResourceState.ERROR -> setupScreenForError(message)

            ResourceState.EMPTY -> setupScreenEmptyState()
        }
    }


    private fun setupSuccess(data: List<GameUiModel>?) {
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressLoad.visibility = GONE
        txtEmpty.visibility = GONE
        data?.let {
            gamesList.addAll(data)
            rvGames.adapter?.notifyDataSetChanged()
        }
    }

    private fun setupScreenEmptyState() {
        progressLoad.visibility = GONE
        rvGames.visibility = GONE
        txtEmpty.visibility = VISIBLE
        txtEmpty.text = "No data found"
    }

    private fun setupScreenForError(message: String?) {
        progressLoad.visibility = GONE
        rvGames.visibility = GONE
        txtEmpty.visibility = VISIBLE
        txtEmpty.text = message
    }


    private fun setupScreenForLoadingState(isLoading: Boolean) {
        txtEmpty.visibility = GONE
        progressLoad.visibility = VISIBLE
        if (isLoading) {
            window?.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        } else {
            progressLoad.visibility = GONE
            window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }
}
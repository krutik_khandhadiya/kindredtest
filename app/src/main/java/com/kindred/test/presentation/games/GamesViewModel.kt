package com.kindred.test.presentation.games

import androidx.lifecycle.LiveData
import com.kindred.test.domain.games.model.GameUiModel
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.presentation.data.Resource
import com.kindred.test.root.BaseViewModel


interface GamesViewModel : BaseViewModel {


    fun getGamesData(gamesRequest: GamesRequest) : LiveData<Resource<List<GameUiModel>>>
}

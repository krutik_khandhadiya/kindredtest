package com.kindred.test.presentation.games.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kindred.test.R
import com.kindred.test.domain.games.model.GameUiModel

class GameListAdapter(val list: List<GameUiModel>) :
    RecyclerView.Adapter<GameListAdapter.GameViewolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewolder {
        val inflater = LayoutInflater.from(parent.context)
        return GameViewolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: GameViewolder, position: Int) {
        val game: GameUiModel = list[position]
        holder.bind(game)
    }

    override fun getItemCount(): Int = list.size

    class GameViewolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.row_game, parent, false)) {
        private var txtTitleGame: TextView? = null
        private var imgGame: ImageView? = null


        init {
            txtTitleGame = itemView.findViewById(R.id.txtGameName)
            imgGame = itemView.findViewById(R.id.imgGame)
        }

        fun bind(gameUiModel: GameUiModel) {
            txtTitleGame?.text = gameUiModel.name
            imgGame?.context?.let {
                Glide.with(it).load(gameUiModel.imageUrl).into(imgGame!!);
            }

        }

    }
}


package com.kindred.test.data.source.games

import com.kindred.test.domain.games.model.GameResponse
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.service.ApiService
import io.reactivex.Flowable
import javax.inject.Inject

class GamesDataRepository
@Inject constructor(private val apiServices: ApiService) {

    fun getGameData(request: GamesRequest): Flowable<GameResponse> {
        return apiServices.getGameInfo(request.getQueryMap())
    }
}
package com.kindred.test.presentation.games

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.kindred.test.domain.games.model.GameUiModel
import com.kindred.test.domain.games.model.GamesRequest
import com.kindred.test.domain.games.usecase.GetGameDataUseCase
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

inline fun <reified T> lambdaMock(): T = mock(T::class.java)

@RunWith(RobolectricTestRunner::class)
class GamesViewModelImplTest {

    @Mock
    lateinit var getGameDataUseCase: GetGameDataUseCase

    lateinit var gamesViewModelImpl: GamesViewModelImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        gamesViewModelImpl =
            GamesViewModelImpl(getGameDataUseCase, CompositeDisposable())
    }

    @Test
    @Throws(Exception::class)
    fun testGetGamesData() {
        val gameResponse = mutableListOf<GameUiModel>()
        gameResponse.add(
            GameUiModel(
                "Sparkling Fruit Match 3",
                "https://a1s.unicdn.net/polopoly_fs/1.1159771.1554472829!/image/2112958919.jpg"
            )
        )
        val observer = lambdaMock<(List<GameUiModel>) -> Unit>()
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        whenever(getGameDataUseCase.execute(GamesRequest()))
            .thenReturn(Flowable.just(gameResponse))


        gamesViewModelImpl.getGamesData(GamesRequest())

        verify(observer).invoke(gameResponse)


    }

    @Test
    fun testGetGamesDataException() {
        val throwable = Throwable("Connection error")

        val observer = lambdaMock<(String) -> Unit>()
        val lifecycle = LifecycleRegistry(mock(LifecycleOwner::class.java))
        lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
        whenever(getGameDataUseCase.execute(GamesRequest()))
            .thenReturn(Flowable.error(throwable))

        gamesViewModelImpl.getGamesData(GamesRequest())

        verify(observer).invoke("Something went wrong")
    }
}
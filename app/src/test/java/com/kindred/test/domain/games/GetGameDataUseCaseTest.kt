package com.kindred.test.domain.games

import com.kindred.test.data.source.games.GamesDataRepository
import com.kindred.test.domain.games.model.*
import com.kindred.test.domain.games.usecase.GetGameDataUseCase
import com.kindred.test.util.RxImmediateSchedulerRule
import com.kindred.test.util.TestSchedulerProvider
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Flowable
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetGameDataUseCaseTest {
    private lateinit var getGameDataUseCase: GetGameDataUseCase

    @Mock
    lateinit var gamesDataRepository: GamesDataRepository


    lateinit var gameResponse: GameResponse

    val gameRequest = GamesRequest()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    @Before
    fun setUp() {

        getGameDataUseCase =
            GetGameDataUseCase(gamesDataRepository, TestSchedulerProvider(), TestSchedulerProvider())

        val gameMap = HashMap<String, GameModel>()

        gameMap.put(
            "10142@greentube", GameModel(
                "10142@greentube", "en_GB",
                SubVendor("Novomatic", "Novomatic"), TypeSettings(
                    95.18,
                    0, "none", 0, "low/medium"
                ),
                "gamevendor.greentube", "/casino/sparkling-fruit-match-3-1.1167559?realMoney=true",
                listOf("fruits", "slots", "themed"), listOf("beAPlusLicense"),
                "Sparkling Fruit Match 3", Vendor("", ""),
                "https://a1s.unicdn.net/polopoly_fs/1.1159771.1554472829!/image/2112958919.jpg", "Novomatic",
                "https://a1s.unicdn.net/polopoly_fs/1.1159778.1554473851!/image/4118373658.jpg"
            )
        )

        gameResponse = GameResponse(gameMap)

    }


    @Test
    fun testExecuteSizeCount() {
        whenever(gamesDataRepository.getGameData(gameRequest))
            .thenReturn(Flowable.just(gameResponse))

        val testObserver = getGameDataUseCase.execute(gameRequest).test()

        testObserver.assertNoErrors()
        testObserver.assertValue { output: List<GameUiModel> -> output.size == 1 }
    }

    @Test
    fun testExecuteName() {
        whenever(gamesDataRepository.getGameData(gameRequest))
            .thenReturn(Flowable.just(gameResponse))

        val testObserver = getGameDataUseCase.execute(gameRequest).test()

        testObserver.assertNoErrors()
        testObserver.assertValue { output: List<GameUiModel> ->
            output[0].name == "Sparkling Fruit Match 3"
                    && output[0].imageUrl == "https://a1s.unicdn.net/polopoly_fs/1.1159771.1554472829!/image/2112958919.jpg"
        }
    }
}
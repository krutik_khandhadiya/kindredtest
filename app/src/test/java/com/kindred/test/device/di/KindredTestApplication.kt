package com.kindred.test.device.di

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class KindredTestApplication : Application(), HasActivityInjector {

    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
        @Inject set

    override fun onCreate() {
        super.onCreate()
        // Initialize Dagger
        DaggerTestAppComponent
            .builder()
            .application(this)
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }
}

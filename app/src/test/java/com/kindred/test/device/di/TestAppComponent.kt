package com.kindred.test.device.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = arrayOf(
        AppModule::class,
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        NetworkModule::class,
        RxModule::class,
        ViewModelModule::class
    )
)
interface TestAppComponent : AndroidInjector<DaggerApplication> {

    fun inject(app: KindredTestApplication)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): TestAppComponent
    }
}

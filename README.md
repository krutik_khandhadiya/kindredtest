# KindredTest

KindredTest demonstrate list of games from unibet apis using MVVM.

Library Used.
  - Rxjava2
  - Retrofit
  - Dagger2
  - Support libraries from Jetpack
  - Glide
  - Roboelectric

# Why MVVM!
 I used to work with MVP pattern until now. But I want to try this View Model in my application. It makes my view independent of ViewModel and lifecycle aware.
 Here the view can observe the datachanges in the viewmodel as we are using LiveData which is lifecycle aware. The viewmodel to view communication is achieved through observer pattern (basically observing the state changes of the data in the viewmodel).
 
 # Dagger2
 Dagger 2 is one of the open source DI frameworks which generates a lot of boilerplate for us. 
 
 # RxJava2
 For combining the reception of requests and results processing and getting rid of standard AsyncTask.
 
 # Glide
 For image load from server.




